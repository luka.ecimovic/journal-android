package hr.ech.journal

import hr.ech.journal.ui.screens.login.validatedPassword
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

class PesswordValidatorTest {
    @Test
    fun emptyPasswordReturnsCorrectString() {
        Assert.assertEquals(
            R.string.error_password_short,
            validatedPassword("").errorMessageId,
        )
    }

    @Test
    fun shortPasswordReturnsCorrectString() {
        Assert.assertEquals(
            R.string.error_password_short,
            validatedPassword("short").errorMessageId,
        )
    }

    @Test
    fun validPasswordPasses() {
        Assert.assertTrue(validatedPassword("hasEverything1!").isValid)
    }
}

@RunWith(Parameterized::class)
class PasswordCharacterTest {
    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun initParameters(): List<Array<Any>> = arrayListOf(
            arrayOf("NO_LOWER_CASE0"),
            arrayOf("no_upper_case_0"),
            arrayOf("no_NUMBERS"),
            arrayOf("noSpecial0"),
        )
    }

    @Parameterized.Parameter(value = 0)
    lateinit var password: String

    @Test
    fun shortPasswordReturnsCorrectString() {
        Assert.assertEquals(
            R.string.error_password_characters,
            validatedPassword(password).errorMessageId,
        )
    }
}
