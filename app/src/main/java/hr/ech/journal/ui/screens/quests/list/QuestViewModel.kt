package hr.ech.journal.ui.screens.quests.list

import android.content.SharedPreferences
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.ech.journal.service.JournalBackendService
import hr.ech.journal.service.QuestDto
import hr.ech.journal.service.getToken
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class QuestViewModel(
    private val dispatcher: CoroutineDispatcher = Dispatchers.Default,
    private val journalBackendService: JournalBackendService,
    private val sharedPreferences: SharedPreferences,
) : ViewModel() {
    val isLoading = mutableStateOf(true)

    val quests = MutableStateFlow(mutableListOf<QuestDto>())

    fun fetchQuests() {
        viewModelScope.launch {
            withContext(dispatcher) {
                quests.compareAndSet(
                    expect = quests.value,
                    update = journalBackendService.listQuests(
                        token = "Bearer ${sharedPreferences.getToken()}",
                    ).toMutableList(),
                )
            }
        }
    }
}
