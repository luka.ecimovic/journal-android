package hr.ech.journal.ui.components

import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation

fun toggledVisualTransformation(toggle: Boolean, transformation: VisualTransformation) =
    if (toggle) {
        transformation
    } else {
        VisualTransformation.None
    }

fun toggledPasswordTransformation(showPassword: Boolean) = toggledVisualTransformation(
    toggle = !showPassword,
    transformation = PasswordVisualTransformation(),
)
