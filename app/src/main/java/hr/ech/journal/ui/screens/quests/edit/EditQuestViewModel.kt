package hr.ech.journal.ui.screens.quests.edit

import android.content.SharedPreferences
import android.os.Parcelable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.ech.journal.service.EditQuestDto
import hr.ech.journal.service.JournalBackendService
import hr.ech.journal.service.getTokenHeader
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize

class EditQuestViewModel(
    private val journalBackendService: JournalBackendService,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Default,
    private val sharedPreferences: SharedPreferences,
) : ViewModel() {
    val questFormData = mutableStateOf(QuestFormData())
    val isValid = mutableStateOf(false)
    val isLoading = mutableStateOf(false)

    fun fetchQuestData(questId: String) {
        isLoading.value = true
        viewModelScope.launch {
            withContext(dispatcher) {
                journalBackendService.fetchQuest(
                    token = sharedPreferences.getTokenHeader(),
                    questId = questId,
                ).let { questDto ->
                    questFormData.value.apply {
                        id = questDto.id
                        title = questDto.title
                        subtitle = questDto.subtitle.orEmpty()
                        description = questDto.description.orEmpty()
                        recurring = questDto.recurring
                        expiresAt = questDto.expiresAt
                        environmentId = questDto.environmentId
                    }
                    isLoading.value = false
                }
            }
        }
    }

    fun submit(
        title: String,
        subtitle: String
    ) {
        viewModelScope.launch {
            withContext(dispatcher) {
                journalBackendService.createQuest(
                    token = sharedPreferences.getTokenHeader(),
                    editQuestDto = EditQuestDto(
                        title = title,
                        shortDescription = subtitle,
                        expires = false,
                    ),
                )
            }
        }
    }
}

@Parcelize
data class QuestFormData(
    var id: String? = null,
    var title: String = "",
    var subtitle: String = "",
    var description: String = "",
    var recurring: Boolean = false,
    var expiresAt: Long? = null,
    var environmentId: String? = null,
    var autoAssign: Boolean = true,
) : Parcelable

fun QuestFormData.toEditQuestDto() = EditQuestDto(
    id = id,
    title = title,
    shortDescription = subtitle,
    longDescription = description,
    expires = recurring,
    expiresAt = expiresAt,
    environmentId = environmentId,
    autoAssign = autoAssign,
)
