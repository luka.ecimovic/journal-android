package hr.ech.journal.ui.screens.login

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import hr.ech.journal.R
import hr.ech.journal.ui.components.LoadingOverlay
import hr.ech.journal.ui.components.TextButton
import hr.ech.journal.ui.components.ValidatedPasswordTextField
import hr.ech.journal.ui.components.ValidatedTextField
import hr.ech.journal.ui.components.initialValidatedTextFieldState
import hr.ech.journal.ui.components.validatedNotEmptyTextField
import org.koin.androidx.compose.koinViewModel

@Composable
fun LoginScreen(
    loginViewModel: LoginViewModel = koinViewModel(),
    onLogin: () -> Unit = {},
) {
    val isLoadingInitial by remember { loginViewModel.isLoadingInitial }
    LoadingOverlay(
        isLoading = isLoadingInitial,
        modifier = Modifier.fillMaxSize(),
    ) {
        LaunchedEffect(Unit) {
            loginViewModel.verifyLogin(onLogin)
        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize(),
        ) {
            val focusManager = LocalFocusManager.current
            var usernameState by rememberSaveable { initialValidatedTextFieldState() }
            var passwordState by rememberSaveable { initialValidatedTextFieldState() }
            val error by rememberSaveable { loginViewModel.error }
            val isLoadingLoginAction by remember { loginViewModel.isLoadingLoginAction }
            error?.let {
                Text(
                    text = it,
                    color = Color.Red,
                )
            }
            Spacer(modifier = Modifier.padding(5.dp))
            ValidatedTextField(
                onValueChange = {
                    usernameState = validatedNotEmptyTextField(it, R.string.lbl_username)
                },
                onDone = { focusManager.moveFocus(FocusDirection.Down) },
                labelIdProvider = { R.string.lbl_username },
                stateProvider = { usernameState },
                enabled = !isLoadingLoginAction,
            )
            ValidatedPasswordTextField(
                onValueChange = { passwordState = validatedPassword(it) },
                onDone = {
                    focusManager.clearFocus()
                    loginViewModel.login(usernameState.text, passwordState.text, onLogin)
                },
                labelIdProvider = { R.string.lbl_password },
                stateProvider = { passwordState },
                enabled = !isLoadingLoginAction,
            )
            Spacer(modifier = Modifier.padding(5.dp))
            TextButton(
                text = stringResource(id = R.string.action_login),
                isLoading = isLoadingLoginAction,
                enabled = usernameState.isValid && passwordState.isValid && !isLoadingLoginAction,
                onClick = {
                    focusManager.clearFocus()
                    loginViewModel.login(usernameState.text, passwordState.text, onLogin)
                },
            )
        }
    }
}

@Preview
@Composable
fun LoginScreenPreview() {
    LoginScreen()
}
