package hr.ech.journal.ui.screens.login

import android.content.SharedPreferences
import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.ech.journal.service.JournalBackendService
import hr.ech.journal.service.LoginDto
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(
    private val journalBackendService: JournalBackendService,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Default,
    private val sharedPreferences: SharedPreferences,
) : ViewModel() {
    val isLoadingInitial = mutableStateOf(true)
    val isLoadingLoginAction = mutableStateOf(false)

    private val isLoggedIn = mutableStateOf(false)

    val error = mutableStateOf<String?>(null)

    fun verifyLogin(onLogin: () -> Unit) {
        isLoggedIn.value = sharedPreferences.getString("journalBackendToken")?.isNotBlank() ?: false
        if (isLoggedIn.value) {
            onLogin()
        } else {
            isLoadingInitial.value = false
        }
    }

    fun login(
        username: String,
        password: String,
        onLogin: () -> Unit,
    ) {
        isLoadingLoginAction.value = true
        error.value = null
        viewModelScope.launch {
            withContext(dispatcher) {
                try {
                    sharedPreferences.edit().putString(
                        "journalBackendToken",
                        journalBackendService.login(
                            LoginDto(
                                email = username,
                                password = password,
                            ),
                        ).apply {
                            Log.d("login result", this.token)
                        }.token,
                    ).commit()
                    withContext(Dispatchers.Main) {
                        onLogin()
                    }
                } catch (e: Throwable) {
                    Log.e("login error", Log.getStackTraceString(e))
                    error.value = "login failed"
                    isLoadingLoginAction.value = false
                }
            }
        }
    }
}

fun SharedPreferences.getString(key: String): String? = getString(key, null)
