package hr.ech.journal.ui.components

import android.os.Parcelable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import hr.ech.journal.R
import kotlinx.parcelize.Parcelize

@Composable
fun TextFieldError(
    stateProvider: () -> ValidatedTextFieldState,
    labelIdProvider: () -> Int,
) {
    if (!stateProvider().isValid) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = stringResource(
                id = stateProvider().errorMessageId,
                stringResource(id = labelIdProvider()),
            ),
            color = MaterialTheme.colorScheme.error,
        )
    }
}

@Parcelize
data class ValidatedTextFieldState(
    val text: String,
    val isValid: Boolean,
    val errorMessageId: Int = 0,
    val errorLabelId: Int = 0,
) : Parcelable

@Composable
fun ValidatedTextField(
    stateProvider: () -> ValidatedTextFieldState,
    labelIdProvider: () -> Int,
    onValueChange: (String) -> Unit,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    onDone: () -> Unit = { },
    autoCorrect: Boolean = true,
    enabled: Boolean = true,
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Default,
    trailingIcon: @Composable () -> Unit = { },
) {
    OutlinedTextField(
        value = stateProvider().text,
        enabled = enabled,
        onValueChange = { onValueChange(it) },
        singleLine = true,
        isError = !stateProvider().isValid,
        visualTransformation = visualTransformation,
        supportingText = { TextFieldError(stateProvider, labelIdProvider) },
        label = { Text(stringResource(id = labelIdProvider())) },
        keyboardOptions = KeyboardOptions.Default.copy(
            autoCorrect = autoCorrect,
            keyboardType = keyboardType,
            imeAction = imeAction,
        ),
        trailingIcon = trailingIcon,
        keyboardActions = KeyboardActions(
            onDone = {
                onDone()
            },
        ),
    )
}

fun initialValidatedTextFieldState() = mutableStateOf(
    ValidatedTextFieldState("", true, 0)
)

@Composable
fun ValidatedPasswordTextField(
    stateProvider: () -> ValidatedTextFieldState,
    labelIdProvider: () -> Int,
    onValueChange: (String) -> Unit,
    enabled: Boolean = true,
    onDone: () -> Unit = { },
) {
    val showPassword = remember { mutableStateOf(false) }
    ValidatedTextField(
        onValueChange = onValueChange,
        onDone = onDone,
        visualTransformation = toggledPasswordTransformation(showPassword.value),
        labelIdProvider = labelIdProvider,
        stateProvider = stateProvider,
        enabled = enabled,
        autoCorrect = false,
        trailingIcon = {
            val (painterResource, iconColor) = if (showPassword.value) {
                Pair(
                    painterResource(R.drawable.outline_visibility_24),
                    MaterialTheme.colorScheme.primary,
                )
            } else {
                Pair(
                    painterResource(R.drawable.outline_visibility_off_24),
                    MaterialTheme.colorScheme.primary,
                )
            }
            IconButton(onClick = { showPassword.value = !showPassword.value }) {
                Icon(
                    painterResource,
                    contentDescription = "Visibility",
                    tint = iconColor,
                )
            }
        },
    )
}
