package hr.ech.journal.ui.screens.quests.edit

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import hr.ech.journal.R
import hr.ech.journal.ui.components.LoadingOverlay
import org.koin.androidx.compose.koinViewModel

@Composable
fun EditQuestScreen(
    editQuestViewModel: EditQuestViewModel = koinViewModel(),
    questId: String? = null,
) {
    questId?.let {
        LaunchedEffect(key1 = Unit) {
            editQuestViewModel.fetchQuestData(it)
        }
    }

    var title by rememberSaveable { mutableStateOf("") }
    var subtitle by rememberSaveable { mutableStateOf("") }
    var expires by rememberSaveable { mutableStateOf(false) }
    var isLoading by rememberSaveable { editQuestViewModel.isLoading }

    LoadingOverlay(isLoading = isLoading) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top,
            modifier = Modifier.fillMaxSize().padding(48.dp),
        ) {
            OutlinedTextField(
                value = title,
                onValueChange = { title = it },
                label = { Text(stringResource(id = R.string.lbl_title)) },
            )
            Spacer(modifier = Modifier.padding(5.dp))
            OutlinedTextField(
                value = subtitle,
                onValueChange = { subtitle = it },
                label = { Text(stringResource(id = R.string.lbl_subtitle)) },
            )
            Spacer(modifier = Modifier.padding(5.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth(),
            ) {
                Text(
                    text = stringResource(id = R.string.lbl_expires),
                    modifier = Modifier.padding(5.dp),
                )
                Checkbox(
                    checked = expires,
                    onCheckedChange = { expires = it },
                )
            }
            if (expires) {
                Spacer(modifier = Modifier.padding(5.dp))
                OutlinedTextField(
                    value = "",
                    onValueChange = { },
                    label = { Text(stringResource(id = R.string.lbl_expiration_date)) },
                )
                Spacer(modifier = Modifier.padding(5.dp))
                OutlinedTextField(
                    value = "",
                    onValueChange = { },
                    label = { Text(stringResource(id = R.string.lbl_expiration_time)) },
                )
            }
            Spacer(modifier = Modifier.padding(5.dp))
            Button(
                onClick = {
                    editQuestViewModel.submit(
                        title,
                        subtitle,
                    )
                },
                enabled = title.isNotBlank() && subtitle.isNotBlank(),
            ) {
                Text(text = stringResource(R.string.action_save))
            }
        }
    }
}
