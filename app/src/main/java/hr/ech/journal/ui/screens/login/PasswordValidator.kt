package hr.ech.journal.ui.screens.login

import hr.ech.journal.R
import hr.ech.journal.ui.components.ValidatedTextFieldState
import org.passay.CharacterRule
import org.passay.EnglishCharacterData
import org.passay.LengthRule
import org.passay.PasswordData
import org.passay.PasswordValidator
import org.passay.RuleResult
import org.passay.RuleResultDetail
import org.passay.WhitespaceRule

fun validatedPassword(password: String) = validatePasswordStrength(password).let { ruleResult ->
    ValidatedTextFieldState(
        password,
        ruleResult.isValid,
        errorMessageId = resolveErrorMesssageId(ruleResult.details),
    )
}

fun validatePasswordStrength(password: String): RuleResult = PasswordValidator(
    listOf(
        LengthRule(8, 20),
        WhitespaceRule(),
        CharacterRule(EnglishCharacterData.UpperCase, 1),
        CharacterRule(EnglishCharacterData.LowerCase, 1),
        CharacterRule(EnglishCharacterData.Digit, 1),
        CharacterRule(EnglishCharacterData.Special, 1),
    )
).validate(PasswordData(password))

fun resolveErrorMesssageId(ruleResultDetails: List<RuleResultDetail>) =
    ruleResultDetails.firstOrNull { it.errorCode == "TOO_SHORT" }?.let {
        R.string.error_password_short
    } ?: R.string.error_password_characters
