package hr.ech.journal.ui.components

import hr.ech.journal.R

fun validatedNotEmptyTextField(
    text: String,
    errorLabelId: Int,
) = ValidatedTextFieldState(
    text = text,
    isValid = text.isNotEmpty(),
    errorMessageId = R.string.error_field_must_not_be_empty,
    errorLabelId = errorLabelId,
)
