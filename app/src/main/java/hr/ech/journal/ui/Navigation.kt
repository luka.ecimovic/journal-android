package hr.ech.journal.ui

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import hr.ech.journal.ui.screens.login.LoginScreen
import hr.ech.journal.ui.screens.login.PasswordScreen
import hr.ech.journal.ui.screens.quests.edit.EditQuestScreen
import hr.ech.journal.ui.screens.quests.list.QuestScreen

@Composable
fun NavigationComponent(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = "login",
    ) {
        composable("auth") {
            AuthorizationNavigationComponent(navController = navController)
        }
        composable("login") {
            LoginScreen {
                navController.navigate("quests")
            }
        }
        composable("quests") {
            QuestScreen { questId ->
                questId?.let {
                    navController.navigate("editQuest/questId={$it}")
                } ?: navController.navigate("editQuest")
            }
        }
        composable(
            route = "editQuest/questId={questId}",
            arguments = listOf(navArgument("questId") { nullable = true }),
        ) { backStackEntry ->
            EditQuestScreen(questId = backStackEntry.arguments?.getString("questId"))
        }
        composable("editQuest") {
            EditQuestScreen()
        }
    }
}

@Composable
fun AuthorizationNavigationComponent(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = "login",
    ) {
        composable("login") {
            LoginScreen {
                navController.navigate("quests")
            }
        }
        composable(
            route = "password",
            enterTransition = {
                slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Left,
                    animationSpec = tween(700),
                )
            },
        ) {
            PasswordScreen()
        }
    }
}
