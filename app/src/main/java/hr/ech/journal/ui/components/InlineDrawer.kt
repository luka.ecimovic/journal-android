package hr.ech.journal.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.KeyboardArrowDown
import androidx.compose.material.icons.outlined.KeyboardArrowUp
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun InlineDrawer(
    titleProvider: ()->String,
    subtitleProvider: ()->String,
    modifier: Modifier = Modifier,
    startCollapsed: Boolean = false,
    content: @Composable ColumnScope.() -> Unit,
) {
    var isColapsed by remember { mutableStateOf(startCollapsed) }
    Card(
        modifier = Modifier.fillMaxWidth(),
    ) {
        Column(
            modifier = modifier,
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
            ) {
                Column(
                    verticalArrangement = Arrangement.SpaceAround
                ) {
                    Text(
                        text = titleProvider(),
                        style = MaterialTheme.typography.bodyLarge,
                        modifier = Modifier.padding(5.dp),
                    )
                    Text(
                        text = subtitleProvider(),
                        style = MaterialTheme.typography.bodyMedium,
                        modifier = Modifier.padding(5.dp),
                    )
                }
                Icon(
                    imageVector = resolveIcon(isColapsed),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(10.dp)
                        .clickable {
                            isColapsed = !isColapsed
                        },
                )
            }
            if (!isColapsed) {
                content()
            }
        }
    }
}

private fun resolveIcon(isCollapsed: Boolean) =
    if (isCollapsed) {
        Icons.Outlined.KeyboardArrowDown
    } else {
        Icons.Outlined.KeyboardArrowUp
    }

@Preview
@Composable
fun DrawerPreview() {
    Surface(
        modifier = Modifier.padding(10.dp)
            .background(MaterialTheme.colorScheme.background),
    ) {
        InlineDrawer(
            titleProvider = { "some title" },
            subtitleProvider = { "some subtitle" },
        ) {
            Text("some content")
        }
    }
}
