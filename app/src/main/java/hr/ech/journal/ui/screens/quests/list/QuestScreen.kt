package hr.ech.journal.ui.screens.quests.list

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import hr.ech.journal.ui.components.LoadingOverlay
import org.koin.androidx.compose.koinViewModel

@Composable
fun QuestScreen(
    questViewModel: QuestViewModel = koinViewModel(),
    editQuest: (String?) -> Unit,
) {
    val isLoading by remember { questViewModel.isLoading }
    LaunchedEffect(Unit) {
        questViewModel.fetchQuests()
    }
    LoadingOverlay(isLoading = isLoading) {
        val quests by questViewModel.quests.collectAsState()
        Surface(
            modifier = Modifier.fillMaxSize(),
        ) {
            LazyColumn {
                items(quests) {
                    QuestListItem(questDto = it, editQuest)
                }
            }
            Column(
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.Bottom,
                modifier = Modifier
                    .padding(16.dp),
            ) {
                FloatingActionButton(
                    onClick = { editQuest(null) },
                    containerColor = MaterialTheme.colorScheme.secondary,
                    shape = RoundedCornerShape(16.dp),
                ) {
                    Icon(
                        imageVector = Icons.Rounded.Add,
                        contentDescription = "Add Quest",
                        tint = Color.White,
                    )
                }
            }
        }
    }
}
