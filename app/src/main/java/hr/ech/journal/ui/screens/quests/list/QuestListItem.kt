package hr.ech.journal.ui.screens.quests.list

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.KeyboardArrowDown
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.ech.journal.service.QuestDto

@Composable
fun QuestListItem(
    questDto: QuestDto,
    editQuest: (String?) -> Unit,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp),
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth(),
        ) {
            Column(
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.padding(12.dp),
            ) {
                Text(
                    text = questDto.title,
                    fontSize = 16.sp,
                )
                Text(questDto.subtitle ?: "")
            }
            Column(
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.padding(12.dp),
            ) {
                Icon(
                    imageVector = Icons.Rounded.KeyboardArrowDown,
                    contentDescription = null,
                )
            }
        }
    }
}

@Preview
@Composable
fun QuestListItemPreview() {
    QuestListItem(
        questDto = QuestDto(
            id = null,
            title = "test title",
            subtitle = "test description",
            questItems = listOf(),
            createdAt = 0L,
            environmentId = "",
            recurring = false,
            expiresAt = 0L,
            description = "",
        ),
        editQuest = {  },
    )
}
