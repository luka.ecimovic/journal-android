package hr.ech.journal.service

import android.content.SharedPreferences
import hr.ech.journal.ui.screens.login.getString

fun SharedPreferences.getToken() = getString("journalBackendToken")

fun SharedPreferences.getTokenHeader() = "Bearer ${getToken()}"
