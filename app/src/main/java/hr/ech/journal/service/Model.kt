package hr.ech.journal.service

data class LoginDto(
    val email: String,
    val password: String,
)

data class RegisterDto(
    val email: String,
    val displayName: String,
    val password: String,
)

data class QuestDto(
    val id: String?,
    val title: String,
    val subtitle: String?,
    val description: String?,
    val createdAt: Long,
    val recurring: Boolean,
    val expiresAt: Long?,
    val environmentId: String?,
    val questItems: List<QuestItemDto>,
)

data class QuestItemDto(
    val id: String,
    val title: String,
    val subtitle: String?,
    val description: String?,
    val type: String,
    val createdAt: Long,
    val recurring: Boolean,
    val expiresAt: Long?,
)

data class EditQuestDto(
    val id: String? = null,
    val title: String,
    val shortDescription: String,
    val longDescription: String? = null,
    val expires: Boolean,
    val expiresAt: Long? = null,
    val environmentId: String? = null,
    val autoAssign: Boolean = true,
)

data class TokenDto(
    val token: String,
)
