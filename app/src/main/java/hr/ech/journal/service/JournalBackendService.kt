package hr.ech.journal.service

import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface JournalBackendService {
    @POST("login")
    suspend fun login(
        @Body loginDto: LoginDto,
    ): TokenDto

    @POST("register")
    suspend fun register(
        @Body registerDto: RegisterDto,
    ): TokenDto

    @GET("quests")
    suspend fun listQuests(
        @Header("Authorization") token: String,
    ): List<QuestDto>

    @GET("quests/{questId}")
    suspend fun fetchQuest(
        @Header("Authorization") token: String,
        @Path("questId") questId: String,
    ): QuestDto

    @POST("quests")
    suspend fun createQuest(
        @Header("Authorization") token: String,
        @Body editQuestDto: EditQuestDto,
    )

    @PUT("quests")
    suspend fun updateQuest(
        @Header("Authorization") token: String,
        @Body editQuestDto: EditQuestDto,
    )
}
