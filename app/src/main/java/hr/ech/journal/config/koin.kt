package hr.ech.journal.config

import hr.ech.journal.service.JournalBackendService
import hr.ech.journal.ui.screens.login.LoginViewModel
import hr.ech.journal.ui.screens.quests.edit.EditQuestViewModel
import hr.ech.journal.ui.screens.quests.list.QuestViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    single {
        Retrofit.Builder()
            .baseUrl("http://192.168.18.6:8080/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(JournalBackendService::class.java)
    }
    single { Dispatchers.IO }
    factory {
        androidApplication().getSharedPreferences("default", android.content.Context.MODE_PRIVATE)
    }
    viewModel { QuestViewModel(get(), get(), get()) }
    viewModel { EditQuestViewModel(get(), get(), get()) }
    viewModel { LoginViewModel(get(), get(), get()) }
}
